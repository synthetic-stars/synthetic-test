extends Node


func list_configs_in_directory(path) -> Array:
	var dir := Directory.new()
	var files: Array = []
	
	if not dir.dir_exists(path):
		return []

	dir.open(path)
	dir.list_dir_begin(true, true)

	while true:
		var file_name: String = dir.get_next()
		
		if file_name == "":
			break
		elif file_name.ends_with(".cfg"):
			files.append(file_name)

	dir.list_dir_end()

	return files


func list_scripts_in_directory(path) -> Array:
	var dir := Directory.new()
	var files: Array = []
	
	if not dir.dir_exists(path):
		return []
	
	dir.open(path)
	dir.list_dir_begin(true, true)

	while true:
		var file_name: String = dir.get_next()
		
		if file_name == "":
			break
		elif file_name.ends_with(".gd"):
			files.append(file_name)

	dir.list_dir_end()

	return files


func list_files_in_directory(path) -> Array:
	var dir := Directory.new()
	var files: Array = []
	
	if not dir.dir_exists(path):
		return []
	
	dir.open(path)
	dir.list_dir_begin(true, true)

	while true:
		var file_name: String = dir.get_next()
		
		if file_name == "":
			break
		else:
			files.append(file_name)

	dir.list_dir_end()

	return files


func list_directories_in_directory(path) -> Array:
	var dirs := []
	var root: Directory = Directory.new()
	
	if not root.dir_exists(path):
		return []

	root.open(path)
	root.list_dir_begin(true, true)

	while true:
		var dir_name: String = root.get_next()
		
		if dir_name == "":
			break
		elif root.current_is_dir():
			dirs.append(dir_name)

	root.list_dir_end()

	return dirs
