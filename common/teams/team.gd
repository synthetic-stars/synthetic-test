class_name Team extends Node

enum TeamColor {
	NEUTRAL,
	RED,
	BLUE,
}

signal score_updated(score_delta)

var team_color: int
var score: int

func _init(team_color: int):
	self.name = "Team" + str(team_color)
	self.team_color = team_color
	self.score = 0
	
	
func add_player(player: Node) -> void:
	player.add_to_group(self.name)


func remove_player(player: Node) -> void:
	player.remove_from_group(self.name)


func update_score(score_delta: int) -> void:
	score += score_delta
	emit_signal("score_updated", score_delta)


func get_players() -> Array:
	return get_tree().get_nodes_in_group(self.name)
	

func get_size() -> int:
	return get_players().size()


#The NEUTRAL team cannot play so it is not counted
static func get_teams_number() -> int:
	return TeamColor.size() - 1
