extends CustomKinematicBody
class_name Player


onready var network_manager: Node = get_node("/root/Root/Networking/NetworkManager")
export(NodePath) onready var energy = get_node(energy)
export(NodePath) onready var health = get_node(health)
export(NodePath) onready var movement = get_node(movement)
export(NodePath) onready var collision = get_node(collision)
export(NodePath) onready var movement_state_machine = get_node(movement_state_machine)
export(NodePath) onready var mesh = get_node(mesh)
export(NodePath) onready var input_system = get_node(input_system)
export(NodePath) onready var weapon_system = get_node(weapon_system)
export(NodePath) onready var head = get_node(head)

#var InputSystem = preload("res://Scenes/Main/Player/InputSystem/InputSystem.gd").new()


