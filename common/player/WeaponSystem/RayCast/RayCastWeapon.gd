extends ReloadWeapon
class_name RayCastWeapon


# Constructor
func init_client(player, type, owner_node, camera, weapon_name, stats, crosshair, id, extras=null) -> void:
	.init_client(player, type, owner_node, camera, weapon_name, stats, crosshair, id, extras)

	Debug.Print("weapons", "Test RayCastWeapon costructor")

func init_server(player, type, owner_node, weapon_name, stats, id, extras=null) -> void:
	.init_server(player, type, owner_node, weapon_name, stats, id, extras)
	 
	Debug.Print("weapons", "Test ReloadWeapon costructor")


func _primary_use() -> bool:
	if ._primary_use():
		# Raycast from the barrel of the gun
		var origin = shoot_origin.global_transform.origin
		var shoot_dir
#		if (ProjectSettings.get_setting("global/server")):
#			shoot_dir = self.player.weapon_system.shoot_dir
#		else:
#
		DrawLine3d.DrawLine(origin, player.target_pos , Color(0, 0, 1), 20)	
		shoot_dir = (player.target_pos - origin).normalized()
		
		if stats["pierce"]:
			var result2 = RaycastUtils.raycast(origin, shoot_dir * stats["fire_distance"], [player])
			if not result2.empty():
				cause_damage(result2.collider)
		else:
			var result2 = RaycastUtils.raycast_pierce(origin, shoot_dir * stats["fire_distance"], [player])
			if not result2.empty():
				for hit in result2:
					cause_damage(hit.collider)
		return true
	return false


func cause_damage(body) -> void:
	if body.get("health"):
		if body.health.has_method("hit"):
			Debug.Print("bullets", "Colliding with player")
			body.health.hit(self.stats.damage)
