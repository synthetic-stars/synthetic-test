extends Area
class_name Projectile

var player : KinematicBody
var projectile_name: String
var stats: Dictionary 

var hit : bool = false
var velocity = Vector3.ZERO

onready var collision_shape = $CollisionShape


# Constructor
func init(player:KinematicBody, projectile_name:String, stats:Dictionary) -> void:
	self.player =  player
	self.projectile_name = projectile_name
	self.stats = stats


# Process physics
func _physics_process(delta):
	if hit:
		return false
	self.stats["time_alive"] -= delta
	if self.stats["time_alive"] < 0:
		hit = true
		Debug.Print("bullets", "Explode because of time")
		return false
	return true


# Process collision
func _on_body_entered(body) -> bool:
	if hit or body == player:
		return false
	Debug.Print("bullets", "Colliding")
	if body.get("health"):
		if body.health.has_method("hit"):
			Debug.Print("bullets", "Colliding with player")
			body.health.hit(stats["damage"])
	collision_shape.disabled = true
	Debug.Print("bullets", "Explode because hit something/someone")
	hit = true
	return true
