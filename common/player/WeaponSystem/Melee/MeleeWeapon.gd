extends ReloadWeapon
class_name MeleeWeapon


func init(player, type, owner_node, camera, weapon_name, stats, crosshair, extras=null) -> void:
	.init(player, type, owner_node, camera, weapon_name, stats, crosshair, extras)

	Debug.Print("weapons", "Test MeleeWeapon costructor")


func _primary_use() -> bool:
	if ._primary_use():		
		return true
	return false
