extends Spatial

"""
Folder structure:
WeaponSystem:{
  Melee:{
	Weapons:{

	}
	MeleeWeapon.gd
  }
  Object:{
	Bullets:{
	  Gravity:{
		Grenade.cfg,
		Grenade.gd,
		Grenade.tscn
	  }
	  NoGravity:{
		Bullet.cfg,
		Bullet.gd,
		Bullet.tscn
	  }
	  gravity_projectile.gd,
	  no_gravity_projectile.gd,
	  projectile.gd
	}
	Weapons:{
	  Bazooka:{
		Bazooka.cfg,
		Bazooka.gd,
		Bazooka.tscn
	  }
	  Grenade:{
		Grenade.cfg,
		Grenade.gd,
		Gremade.tscn
	  }
	}
	ObjectWeapon.gd
  }
  RayCast:{
	Weapons:{
	  Gun:{
		Gun.cfg,
		Gun.gd,
		Gun.tscn
	  }
	  SmallGun:{
		SmallGun.cfg,
		SmallGun.gd,
		SmallGun.tscn
	  }
	}
	RayCastWeapon.gd
  }
  ReloadWeapon.gd,
  Weapon.gd
}
"""


onready var player := get_parent().get_parent()
export(NodePath) onready var input_system = get_node(input_system)

var arsenal : Dictionary;
var prev : int = 0
var current : int = 0;

var shoot_dir

var crosshair : TextureRect
var camera : Camera

func _ready() -> void:
	var function
	if (ProjectSettings.get_setting("global/server")):
		function = funcref(self, "_change_weapon_server")
	else:
		function = funcref(self, "_change_weapon_client")
	
	crosshair = $"../../FollowingCamera/Crosshair"
	camera = $"../../FollowingCamera/Interpolated/Camera"
	input_system.register_function(["scroll_wheel_up", "scroll_wheel_down", "KEY_1", "KEY_2", "KEY_3"], "just_pressed", function)

	_load_weapons()

	for w in arsenal:
		arsenal.values()[current]._hide();


func _load_weapons() -> void:
	var weapon_sys_path = "res://common/player/WeaponSystem/" # cartella root
	var preferences_path = weapon_sys_path + "WeaponsPreferences.cfg"
	var preferences_config = ConfigFile.new()
	var err = preferences_config.load(preferences_path)

	if err == OK:
		var preferences = preferences_config.get_value("Weapons", "preferences")
		var categories_dirs = DirUtils.list_directories_in_directory(weapon_sys_path)
		Debug.Print("weapons", categories_dirs)

		# per ogni cartella di ogni tipo di arma
		for category in categories_dirs:
			# cartella delle armi in ogni cartella corrispondente al tipo di arma
			var weapons_dirs = DirUtils.list_directories_in_directory(weapon_sys_path + category + "/Weapons/")

			# per ogni arma
			for weapon_dir in weapons_dirs:
				var config = ConfigFile.new()
				err = config.load(weapon_sys_path + category + '/Weapons/' + weapon_dir + '/' + weapon_dir + '.cfg')

				if err == OK:
					var name = config.get_value("Details", "name")

					if preferences.has(name):
						# imposta le proprietà dei dettagli
						var gd_name = config.get_value("Details", "gd_name")
						var type = config.get_value("Details", "type") # 0 RayCast, 1 Object, 2 Melee
						var id = config.get_value("Details", "id")

						# inizializza le var per le altre due sezioni del config
						var stats : Dictionary
						var extras = null

						# riempi il dizionario delle stats
						for key in config.get_section_keys("Stats"):
							stats[key] = config.get_value("Stats", key)

						# riempi gli extra se ci sono
						if config.has_section("Extra"):
							var extras_keys = config.get_section_keys("Extra")
							extras = {}

							for key in extras_keys:
								var extra_value = config.get_value("Extra", key)
								extras[key] = extra_value

						# instanzia e imposta la scena dell'arma
						var weapon_buffer = load(weapon_sys_path + category + '/Weapons/' + weapon_dir + '/' + weapon_dir + '.tscn')
						var weapon_instance = weapon_buffer.instance()

						if (ProjectSettings.get_setting("global/server")):
							weapon_instance.init_server(player, type, self, gd_name, stats, id, extras)
						else:
							weapon_instance.init_client(player, type, self, camera, gd_name, stats, crosshair, id, extras)

						weapon_instance.global_transform = global_transform
						weapon_instance.transform = transform
						add_child(weapon_instance)
						weapon_instance.extras = extras
						arsenal[gd_name] = weapon_instance
						var function = funcref(weapon_instance, "_reload")
						input_system.register_function(["reload"], "just_pressed", function)
						function = funcref(weapon_instance, "_primary_use")
						input_system.register_function(["primary_fire"], "just_pressed", function)

						if extras:
							for extra in extras:
								function = funcref(weapon_instance, extras[extra])
								input_system.register_function([extra], "just_pressed", function)


func _change() -> void:
	# Change weapons
	for w in range(arsenal.size()):
		if arsenal.values()[w] != arsenal.values()[current]:
			arsenal.values()[w]._hide();
		else:
			arsenal.values()[w]._draw();


func _change_weapon_client() -> void:
	if arsenal.values()[current]._can_change():
		var changed = false
		if player.input_system.get_state_status("scroll_wheel_up","just_pressed"):
			current = (current - 1) if (current > 0) else 2
			changed = true
		if player.input_system.get_state_status("scroll_wheel_down","just_pressed"):
			current = (current + 1) if (current < 2) else 0
			changed = true
		if player.input_system.get_state_status("KEY_1","just_pressed"):
			current = 0
			changed = true
		if player.input_system.get_state_status("KEY_2","just_pressed"):
			current = 1
			changed = true
		if player.input_system.get_state_status("KEY_3","just_pressed"):
			current = 2
			changed = true
		if changed:
			self._change()


func _change_weapon_server() -> void:
	if arsenal.values()[current]._can_change():
		var changed = false
		if prev != current:
			changed = true
			prev = current
		if changed:
			self._change()
