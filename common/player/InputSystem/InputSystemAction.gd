class_name InputSystemAction


var name: String
var required_clicks: int
var max_click_delay: int
var input_system_category : String 

var clicks: int = 0
var last_timestamp: int = 0
var states: Array

var full_name: String

const just_pressed = 0b001
const pressed = 0b010
const just_released = 0b100

var prev_buffer = 0b000 #The buffer with the states before the current cycle


func _to_string() -> String:
	var final_string = "Name: %s, req clicks: %s, max ms: %s, full name: %s" % [self.name, self.required_clicks, self.max_click_delay, self.full_name]
	final_string += "\n    States: "
	for state in self.states:
		final_string += "\n        "
		final_string+= state._to_string()
	return final_string


func _init(action_name: String) -> void:
	var parsed_action_name: Array = action_name.split("_")

	self.name = action_name

	if parsed_action_name.size() < 4: # Per evitare i default
		self.required_clicks = 1
		self.max_click_delay = 0
		self.full_name = action_name
		self.input_system_category = "p"
	else:
		self.name.erase(0, parsed_action_name[1].length() + parsed_action_name[2].length() + 4)	
		self.required_clicks = int(parsed_action_name[1])
		self.max_click_delay = int(parsed_action_name[2])
		self.full_name = parsed_action_name[0] + "_" + str(self.required_clicks) + "_" + str(self.max_click_delay) + "_" + self.name
		self.input_system_category = parsed_action_name[0]

	init_states()


func init_states() -> void:
	states.append(InputSystemState.new("just_pressed"))
	states.append(InputSystemState.new("pressed"))
	states.append(InputSystemState.new("just_released"))


func get_state(name:String):
	for state in states:
		if state.name == name:
			return state


func state_is_enabled(b, flag):
	return b & flag != 0


func set_state(b, flag):
		b = b|flag
		return b


func unset_state(b, flag):
		b = b & ~flag
		return b

		
func process_server(current_time:int) -> void:
	for state in states:
		if state.value:
			state.call_functions()


func process_client(current_time:int) -> bool:
	var buffer = 0b000

	for state in states:
		if Input.callv("is_action_" + state.name, [full_name]):
			if state.name == "just_pressed":
				if current_time - last_timestamp > max_click_delay:
					clicks = 0

				clicks = clamp(clicks + 1, 0, required_clicks)
				last_timestamp = current_time

				if clicks == required_clicks:
					buffer = set_state(buffer, get(state.name))
					state.value = true
					state.call_functions()

			elif state.name == "pressed":
				if (buffer == 0b001): 
					buffer = set_state(buffer, get(state.name))
					state.call_functions()
					state.value = true

				if (prev_buffer == 0b010 || prev_buffer == 0b011): 
					buffer = set_state(buffer, get(state.name))
					state.call_functions()

			else:
				if (prev_buffer == 0b010 || prev_buffer == 0b011):
					buffer = set_state(buffer, get(state.name))
					state.value = true
					state.call_functions()

		else:
			state.value = false

	prev_buffer = buffer
	return buffer


func get_state_status(name:String)->bool:
	for state in states:
		if state.name == name:
			return state.value
	return false


func set_state_status(bitmask) -> void:
	for state in states:
		if (state_is_enabled(bitmask, get(state.name))):
			state.value = true
		else:
			state.value = false

func unset_state_status(bitmask) -> void:
	for state in states:
		if (state_is_enabled(bitmask, get(state.name))):
			state.value = false
				
	
