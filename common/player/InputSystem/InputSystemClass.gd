class_name InputSystemClass

var _input_systems: Array

var _actions: Dictionary = {
	p_1_slide = 0b000,
	u_1_0_primary_fire = 0b000,
	p_1_0_jump = 0b000,
	p_1_0_forward = 0b000,
	p_2_300_dash = 0b000,
	p_1_0_backward = 0b000,
	p_1_0_right = 0b000,
	p_1_0_left = 0b000,
	p_1_0_shift = 0b000,
	u_1_0_reload = 0b000,
	u_1_0_right_click = 0b000
}

"""
Stati: 0 Just Pressed, 1 Pressing, 2 Just Released
_inputs = [
	{"walking":{
		states: [
			{ pressed: false, func: "func" },
			{ just_pressed: false, func: "func" },
			{ just_released: false, func: "func" }
		],
		required_clicks: 1, // just_pressed
		max_click_delay: 300, // just_pressed
		clicks: 0,
		last_timestamp: 15654 // just_pressed
	}},

	{"forward":{
		states: [
			{ pressed: true, func: null },
		]
		required_clicks: 1, // just_pressed
		max_click_delay: 300, // just_pressed
		clicks: 0,
		last_timestamp: 0 // just_pressed
	}

	{"running": {
		states: [
			{ pressed: false, func: "func" },
			{ just_pressed: false, func: "func" },
			{ just_released: false, func: "func" }
		],
		required_clicks: 2, // just_pressed
		max_click_delay: 300, // just_pressed
		clicks: 0,
		last_timestamp: 15654 // just_pressed
	}}
]
"""


func _init() -> void:
	_input_systems.append(InputSystemActions.new("u"))
	_input_systems.append(InputSystemActions.new("p"))

	for action in InputMap.get_actions():
		var input_system_action: InputSystemAction = InputSystemAction.new(action)
		var input_system: InputSystemActions

		match input_system_action.input_system_category:
			"u":
				input_system = _input_systems[0]
			"p":
				input_system = _input_systems[1]
		
		input_system.add_action(input_system_action)


func reset_actions() -> void:
	for action in _actions:
		_actions[action] = 0b000


func register_function(actions_array: Array, state_name: String, function_ref: FuncRef) -> void:
	for input_system in _input_systems:
		for action in input_system.actions:
			if (actions_array.has(action.name)):
				action.get_state(state_name).add_func(function_ref)


func process_actions(input: CPacketInput) -> void:
	var actions_keys = _actions.keys()
	
	for i in range(0, input.input_actions_length()):
		var input_action = input.input_actions(i)
		_actions[actions_keys[input_action.name()]] = input_action.states()
		
	
	for i in range(0, _input_systems.size()):
		var _input_system = _input_systems[i]
		
		for input_system_action in _input_system.actions:
			var action_bitmask = _actions.get(input_system_action.full_name) # Se non esiste ritorna null
			if action_bitmask != null:
				input_system_action.set_state_status(action_bitmask)
		
		_execute_functions_server(i)


func _unhandled_input() -> void:
	_execute_functions_client(0)


func _physics_process() -> void:
	_execute_functions_client(1)


func _execute_functions_server(index: int) -> void:
	var current_time: int = OS.get_ticks_msec()

	for action in _input_systems[index].actions:
		action.process_server(current_time)


func _execute_functions_client(index: int) -> void:
	var current_time: int = OS.get_ticks_msec()
	
	for action in _input_systems[index].actions:
		var bitmask = action.process_client(current_time)
		
		if _actions.has(action.full_name):
			_actions[action.full_name] |= bitmask


func is_just_pressed(action:String) -> bool:
	return self.get_state_status(action, "just_pressed")


func is_pressed(action:String) -> bool:
	return self.get_state_status(action, "pressed")


func is_just_released(action:String) -> bool:
	return self.get_state_status(action, "just_released")


func get_state_status(action:String, state:String) -> bool:
	for input_system in _input_systems:
		var action_buffer = input_system.get_action_by_name(action)
		if  action_buffer != null:
			return action_buffer.get_state_status(state)

	return false


func action_press_client(action:String) -> void:
	var action_buffer = null
	var i = 0
	while action_buffer == null:
		action_buffer = _input_systems[i].get_action_by_name(action)
		i += 1

	if action_buffer != null:
		Input.action_press(action_buffer.full_name)


func action_release_client(action:String) -> void:
	var action_buffer = null
	var i = 0
	while action_buffer == null:
		action_buffer = _input_systems[i].get_action_by_name(action)
		i += 1

	if action_buffer != null:
		Input.action_release(action_buffer.full_name)


func action_press_server(action:String) -> void:
	var action_buffer = null
	var i = 0
	while action_buffer == null:
		action_buffer = _input_systems[i].get_action_by_name(action)
		i += 1

	if action_buffer != null:
		action_buffer.set_state_status(0b011)


func action_release_server(action:String) -> void:
	var action_buffer = null
	var i = 0
	while action_buffer == null:
		action_buffer = _input_systems[i].get_action_by_name(action)
		i += 1

	if action_buffer != null:
		action_buffer.unset_state_status(0b011)

