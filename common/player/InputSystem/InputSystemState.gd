class_name InputSystemState


var value: bool = false
var name: String
var callbacks: Array


func _init(name:String) -> void:
	self.name = name


func _to_string() -> String:
	return "Name: " + self.name + ", callbacks: " + str(self.callbacks)


func add_func(function:FuncRef) -> void:
	callbacks.append(function)


func call_functions() -> void:
	if callbacks:
		for callback in callbacks:
			callback.call_func()
