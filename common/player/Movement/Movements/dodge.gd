extends Movement
# A force impulse to the right/left that
# consumes a fixed amount of energy.

var dodge_force := 170
var dodge_energy_consumption := 20.0
var deceleration := 9
var _dodged_in_air := false


func act() -> void:
	player.set_velocity(Vector3.ZERO)

	player.energy.decrease(dodge_energy_consumption, "dodge")

	if player.input_system.is_pressed("right"):
		player.add_force_relative_to_camera(Vector3.RIGHT * dodge_force)
	elif player.input_system.is_pressed("left"):
		player.add_force_relative_to_camera(Vector3.LEFT * dodge_force)


func has_to_dodge() -> bool:
	var is_moving_laterally: bool = (
		player.input_system.is_pressed("right")
		or player.input_system.is_pressed("left")
	)
	var is_moving_forward_or_back: bool = (
		player.input_system.is_pressed("forward")
		or player.input_system.is_pressed("dash")
		or player.input_system.is_pressed("backward")
	)
	var has_enough_energy = player.energy._energy > dodge_energy_consumption
	var is_pressing_the_right_keys: bool = (
		player.input_system.is_just_pressed("jump")
		and not is_moving_forward_or_back
	)

	var can_dodge: bool = is_moving_laterally and has_enough_energy and is_pressing_the_right_keys

	if player.is_on_floor():
		return can_dodge
	else:
		return can_dodge and not _dodged_in_air


func decelerate(delta) -> void:
	player.apply_gravity(delta)
	player.move_horizontally(delta, deceleration)
