extends Node


export var max_energy := 100.0
export var energy_refill_duration := 6

var _energy: float = max_energy
var _descreasing_jobs: Dictionary
var _increasing_jobs: Dictionary


func set_energy(new_energy: float) -> void:
	_energy = clamp(new_energy, 0, max_energy)


func decrease(value: float, job_name: String, delay: float = 0) -> void:
	_edit_energy(-value, _descreasing_jobs, job_name, delay)


func increase(value: float, job_name: String, delay: float = 0) -> void:
	_edit_energy(value, _increasing_jobs, job_name, delay)


func _edit_energy(value: float, jobs_dictionary: Dictionary, job_name: String, delay: float) -> void:
	if jobs_dictionary.has(job_name):
		return
	jobs_dictionary[job_name] = true
	
	if delay != 0:
		yield(get_tree().create_timer(delay), "timeout")
	
	set_energy(_energy + value)
	
	jobs_dictionary.erase(job_name)


func _physics_process(_delta: float):
	if _descreasing_jobs.size() == 0:
		_refill_energy()


func _refill_energy() -> void:
	var energy_gain := max_energy / energy_refill_duration * 0.1
	increase(energy_gain, "refill", 0.1)
