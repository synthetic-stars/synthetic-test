class_name FlatBuffersBuilder
extends Reference

const Utils       = preload("Utils.gd")
const NumberTypes = preload("NumberTypes.gd")
const Constants   = NumberTypes.Constants

var _buffer: StreamPeerBuffer
var _space: int
var _minalign: int = 1
var _vtable: Array
var _vtable_in_use: int = 0
var _nested: bool = false
var _finished: bool = false
var _object_start: int
var _vtables: Array
var _num_vTables: int = 0
var _vector_num_elems: int = 0
var _force_defaults: bool = false

const DEFAULT_BUFFER_SIZE: int = 1024
const MAX_BUFFER_SIZE: int = int(pow(2, 31)) - 8

func _init(initial_size: int = DEFAULT_BUFFER_SIZE):
	if initial_size <= 0:
		initial_size = DEFAULT_BUFFER_SIZE
		
	self._buffer = StreamPeerBuffer.new()
	self._buffer.set_data_array(PoolByteArray(Utils.create_valued_array(initial_size, 0)))
	self._space = _buffer.get_size()
	self._vtables.resize(16)


func clear():
	#TO implement
	pass


func grow_byte_buffer(buffer: StreamPeerBuffer) -> StreamPeerBuffer:
	var old_buf_size: int = buffer.get_size()

	if (old_buf_size == MAX_BUFFER_SIZE): # Ensure we don't grow beyond what fits in an int.
		print("FlatBuffers: cannot grow buffer beyond 2 gigabytes.")
		return buffer
		
	var new_buf_size: int = min(old_buf_size * 2, MAX_BUFFER_SIZE)
	if new_buf_size == 0:
		new_buf_size = 1
	
	var new_buf: PoolByteArray
	new_buf = PoolByteArray(Utils.create_valued_array(new_buf_size-old_buf_size, 0))
	new_buf.append_array(buffer.get_data_array())
	buffer.set_data_array(new_buf)
	return buffer


func offset() -> int:
	"""Offset relative to the end of the buffer."""
	return _buffer.get_size() - _space


func pad(byte_size: int):
	"""Add zero valued bytes to prepare a new entry to be added.
	
	Parameters
	----------
	byte_size : int
		Number of bytes to add.
	"""
	for i in range(byte_size):
		put(NumberTypes.Uint8Flags, 0)


func prep(size: int, additional_bytes: int):
	"""Prep prepares to write an element of `size` after `additional_bytes`
	have been written, e.g. if you write a string, you need to align
	such the int length field is aligned to SizeInt32, and the string
	data follows it directly.
	If all you need to do is align, `additionalBytes` will be 0.
	"""
	
	# Track the biggest thing we've ever aligned to.
	if (size > _minalign):
		_minalign = size
	# Find the amount of alignment needed such that `size` is properly
	# aligned after `additional_bytes`
	var align_size: int = ((~(offset() + additional_bytes)) + 1) & (size - 1)
	# Reallocate the buffer if needed.
	while (_space < align_size + size + additional_bytes):
		var old_buf_size: int = _buffer.get_size()
		_buffer = grow_byte_buffer(_buffer)
		_space += _buffer.get_size() - old_buf_size
	pad(align_size)

##########################################################

func put(flags, x):
	"""Place prepends a value specified by `flags` to the Builder,
	without checking for available space.
	"""
	NumberTypes.enforce_value(flags, x)
	_space -= flags.bytewidth
	_buffer.seek(_space)
	
	match flags.name:
		"bool":
			_buffer.put_8(1 if x else 0)
		"int8":
			_buffer.put_8(x)
		"int16":
			_buffer.put_16(x)
		"int32":
			_buffer.put_32(x)
		"int64":
			_buffer.put_64(x)
		"uint8":
			_buffer.put_u8(x)
		"uint16":
			_buffer.put_u16(x)
		"uint32":
			_buffer.put_u32(x)
		"uint64":
			_buffer.put_u64(x)
		"float32":
			_buffer.put_float(x)
		"float64":
			_buffer.put_double(x)


func put_UOffsetT(x: int):
	"""PlaceUOffsetT prepends a UOffsetT to the Builder, without checking
	for space.
	"""
	put(NumberTypes.UOffsetTFlags, x)


#######################################################

func start_vector(elem_size: int, num_elems: int, alignment: int) -> int:
	"""Start a new array/vector of objects.  Users usually will not call
	this directly.  The `FlatBuffers` compiler will create a start/end
	method for vector types in generated code.
	
	The expected sequence of calls is:
 	- Start the array using this method.
	- Call addOffset(int) `num_elems` number of times to set
	  the offset of each element in the array.
	- Call endVector() to retrieve the offset of the array.
	
	Parameters
	----------
	elem_size : int
		The size of each element in the array.
	num_elems : int
		The number of elements in the array.
	alignment : int
		The alignment of the array.
	"""
	not_nested()
	_nested = true
	_vector_num_elems = num_elems
	prep(NumberTypes.UOffsetTFlags.bytewidth, elem_size * num_elems)
	prep(alignment, elem_size * num_elems) # Just in case alignment > int.
	return offset()


func end_vector() -> int:
	"""Finish off the creation of an array and all its elements.  The array
	must be created with {@link #startVector(int, int, int)}.
	
	Returns
	-------
	int
		The offset at which the newly created array starts.
	@see #startVector(int, int, int)
	"""
	if not _nested:
		print("FlatBuffers: endVector called without startVector")
		return -1

	_nested = false
	put_UOffsetT(_vector_num_elems)
	_vector_num_elems = 0
	return offset()


func create_string(s: String) -> int:
	var length: int = s.length()
	prepend_byte(0)
	start_vector(1, length, 1)
	_space -= length
	_buffer.seek(_space)
	_buffer.put_data(s.to_utf8())
	return end_vector()


func create_byte_vector(arr: Array) -> int:
	var length = arr.size()
	start_vector(1, length, 1)
	_space -= length
	_buffer.seek(_space)
	_buffer.put_data(arr)
	return end_vector()


func finished():
	"""Should not be accessing the final buffer before it is finished."""
	assert(_finished, "FlatBuffers: you can only access the serialized buffer after it has been" +
				" finished by FlatBufferBuilder.finish().")
	if not _finished:
		print("FlatBuffers: you can only access the serialized buffer after it has been" +
				" finished by FlatBufferBuilder.finish().")


func not_nested():
	"""Should not be creating any other object, string or vector
	while an object is being constructed.
	"""
	assert(not _nested, "FlatBuffers: object serialization must not be nested.")
	if _nested:
		print("FlatBuffers: object serialization must not be nested.")


func assert_struct_is_inline(obj: int):
	"""Structures are always stored inline, they need to be created right
	where they're used.  You'll get this assertion failure if you
	created it elsewhere.
	
	Parameters
	----------
	obj : int
		The offset of the created object.
	"""
	NumberTypes.enforce_value(NumberTypes.UOffsetTFlags, obj)
	assert(obj == offset(), "FlatBuffers: struct must be serialized inline.")
	if obj != offset():
		print("FlatBuffers: struct must be serialized inline.")


func start_table(numfields: int):
	not_nested()
	if _vtable.size() < numfields:
		_vtable.resize(numfields)
		
	_vtable_in_use = numfields
	#forse non necessario fill_array
	Utils.fill_array(_vtable, 0, _vtable_in_use, 0)
	_nested = true
	_object_start = offset()


func slot(voffset: int):
	assert(voffset < _vtable.size(), "Flatbuffers: invalid voffset")
	_vtable[voffset] = offset()


func end_table() -> int:
	assert(_nested, "FlatBuffers: endTable called without startTable")
	prepend_int32(0)
	
	var vtableloc: int = offset()
	# Write out the current vtable.
	
	# Trim trailing zeroes
	var j: int = _vtable_in_use - 1
	while (j >= 0 and _vtable[j] == 0):
		j -= 1

	var trimmed_size: int = j + 1
	while j >= 0:
		# Offset relative to the start of the table.
		var off: int = (vtableloc - _vtable[j]) if _vtable[j] != 0 else 0
		prepend_uint16(off)
		j -= 1

	# The two metadata fields are written last.
	var standard_fields: int = 2
	# First, store the object bytesize:
	prepend_uint16(vtableloc - _object_start)
	# Second, store the vtable bytesize:
	prepend_uint16((trimmed_size + standard_fields) * NumberTypes.VOffsetTFlags.bytewidth)
	
	var existing_vtable: int = 0
	for i in range(_num_vTables):
		var vt1: int = _buffer.get_size() - _vtables[i]
		var vt2: int = _space
		
		_buffer.seek(vt1)
		var vt1_len:int = _buffer.get_16()
		_buffer.seek(vt2)
		var vt2_len:int = _buffer.get_16()
		if vt1_len == vt2_len:
			if compare_vtables(vt1, vt2, vt1_len):
				existing_vtable = _vtables[i]
				break

	if existing_vtable != 0:
		# Found a match:
		# Remove the current vtable.
		_space = _buffer.get_size() - vtableloc
		# Point table to existing vtable.
		_buffer.seek(_space)
		_buffer.put_32(existing_vtable - vtableloc)
	else:
		# No match:
		# Add the location of the current vtable to the list of vtables.
		if _num_vTables == _vtables.size():
			_vtables.resize(_num_vTables * 2)
			
		_vtables[_num_vTables] = offset()
		_num_vTables += 1
		# Point table to current vtable.
		_buffer.seek(_buffer.get_size() - vtableloc)
		_buffer.put_32(offset() - vtableloc)
		
	_nested = false
	return vtableloc


func compare_vtables(vt1: int, vt2: int, vt_len: int) -> bool:
	for i in range(Constants.SIZEOF_SHORT, vt_len, Constants.SIZEOF_SHORT):
		_buffer.seek(vt1 + i)
		var val1: int = _buffer.get_16()
		_buffer.seek(vt2 + i)
		var val2: int = _buffer.get_16()
		if val1 != val2:
			return false
	return true


func _finish(root_table: int, size_prefix: bool):
	prep(_minalign, Constants.SIZEOF_INT + (Constants.SIZEOF_INT if size_prefix else 0))
	prepend_UOffsetTRelative(root_table)
	if size_prefix:
		prepend_int32(_buffer.get_size() - _space)
	_buffer.seek(_space)
	_finished = true


func finish(root_table: int):
	_finish(root_table, false)


func output() -> StreamPeerBuffer:
	"""Get the StreamPeerBuffer representing the FlatBuffer. Only call this after you've
	called `finish()`. The actual data starts at the StreamPeerBuffer's current position,
	not necessarily at `0`.
	"""
	finished()
	return _buffer


##################################################
func prepend(flags, x):
	prep(flags.bytewidth, 0)
	put(flags, x)


func prepend_bool(x: bool):
	"""Prepend a `bool` to the Builder buffer.
	Note: aligns and checks for space.
	"""
	prepend(NumberTypes.BoolFlags, x)


func prepend_byte(x: int):
	"""Prepend a `byte` to the Builder buffer.
	Note: aligns and checks for space.
	"""
	prepend(NumberTypes.Uint8Flags, x)


func prepend_uint8(x: int):
	"""Prepend an `uint8` to the Builder buffer.
	Note: aligns and checks for space.
	"""
	prepend(NumberTypes.Uint8Flags, x)


func prepend_uint16(x: int):
	"""Prepend an `uint16` to the Builder buffer.
	Note: aligns and checks for space.
	"""
	prepend(NumberTypes.Uint16Flags, x)


func prepend_uint32(x: int):
	"""Prepend an `uint32` to the Builder buffer.
	Note: aligns and checks for space.
	"""
	prepend(NumberTypes.Uint32Flags, x)


func prepend_uint64(x: int):
	"""Prepend an `uint64` to the Builder buffer.
	Note: aligns and checks for space.
	"""
	prepend(NumberTypes.Uint64Flags, x)


func prepend_int8(x: int):
	"""Prepend an `int8` to the Builder buffer.
	Note: aligns and checks for space.
	"""
	prepend(NumberTypes.Int8Flags, x)


func prepend_int16(x: int):
	"""Prepend an `int16` to the Builder buffer.
	Note: aligns and checks for space.
	"""
	prepend(NumberTypes.Int16Flags, x)


func prepend_int32(x: int):
	"""Prepend an `int32` to the Builder buffer.
	Note: aligns and checks for space.
	"""
	prepend(NumberTypes.Int32Flags, x)


func prepend_int64(x: int):
	"""Prepend an `int64` to the Builder buffer.
	Note: aligns and checks for space.
	"""
	prepend(NumberTypes.Int64Flags, x)


func prepend_float32(x: float):
	"""Prepend a `float` to the Builder buffer.
	Note: aligns and checks for space.
	"""
	prepend(NumberTypes.Float32Flags, x)


func prepend_float64(x: float):
	"""Prepend a `double` to the Builder buffer.
	Note: aligns and checks for space.
	"""
	prepend(NumberTypes.Float64Flags, x)


func prepend_UOffsetTRelative(off: int):
	"""Prepends an unsigned offset into vector data, relative to where it
	will be written.
	"""
	prep(NumberTypes.UOffsetTFlags.bytewidth, 0)
	assert(off <= offset(), "flatbuffers: Offset arithmetic error.")
	off = offset() - off + NumberTypes.UOffsetTFlags.bytewidth
	put_UOffsetT(off)
	

#########################################################

func prepend_slot(flags, o: int, x, d: int):
	"""Add to a table at `o` into its vtable, with value `x` and default `d`."""
	NumberTypes.enforce_value(flags, x)
	NumberTypes.enforce_value(flags, d)
	if _force_defaults || x != d:
		prepend(flags, x)
		slot(o)


func prepend_bool_slot(o: int, x: bool, d: int):
	prepend_slot(NumberTypes.BoolFlags, o, x, d)


func prepend_byte_slot(o: int, x: int, d: int):
	prepend_slot(NumberTypes.Uint8Flags, o, x, d)


func prepend_uint8_slot(o: int, x: int, d: int):
	prepend_slot(NumberTypes.Uint8Flags, o, x, d)


func prepend_uint16_slot(o: int, x: int, d: int):
	prepend_slot(NumberTypes.Uint16Flags, o, x, d)


func prepend_uint32_slot(o: int, x: int, d: int):
	prepend_slot(NumberTypes.Uint32Flags, o, x, d)


func prepend_uint64_slot(o: int, x: int, d: int):
	prepend_slot(NumberTypes.Uint64Flags, o, x, d)


func prepend_int8_slot(o: int, x: int, d: int):
	prepend_slot(NumberTypes.Int8Flags, o, x, d)


func prepend_int16_slot(o: int, x: int, d: int):
	prepend_slot(NumberTypes.Int16Flags, o, x, d)


func prepend_int32_slot(o: int, x: int, d: int):
	prepend_slot(NumberTypes.Int32Flags, o, x, d)


func prepend_int64_slot(o: int, x: int, d: int):
	prepend_slot(NumberTypes.Int64Flags, o, x, d)


func prepend_float32_slot(o: int, x: float, d: int):
	prepend_slot(NumberTypes.Float32Flags, o, x, d)


func prepend_float64_slot(o: int, x: float, d: int):
	prepend_slot(NumberTypes.Float64Flags, o, x, d)


func prepend_UOffsetTRelative_slot(o: int, x: int, d: int):
	"""
	PrependUOffsetTRelativeSlot prepends an UOffsetT onto the object at
	vtable slot `o`. If value `x` equals default `d`, then the slot will
	be set to zero and no other data will be written.
	"""
	if _force_defaults || x != d:
		prepend_UOffsetTRelative(x)
		slot(o)


func prepend_struct_slot(voffset: int, x: int, d: int):
	NumberTypes.enforce_value(NumberTypes.UOffsetTFlags, d)
	if x != d:
		assert_struct_is_inline(x)
		slot(voffset)

#########################################################

func sized_pool_byte_array(start: int = _space, 
						length: int = (_buffer.get_size() - _space)) -> Array:
	finished()
	_buffer.seek(start)
	var ret = _buffer.get_partial_data(length)
	var err_code = ret[0]
	assert (err_code == OK, "get_partial_data() error %s" % err_code)
	return ret[1]
