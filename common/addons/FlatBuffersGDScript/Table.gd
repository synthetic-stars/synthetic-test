# All tables in the generated code derive from this class, and add their own accessors.
class_name Table
extends Reference

const Utils       = preload("Utils.gd")
const NumberTypes = preload("NumberTypes.gd")

# The underlying StreamPeerBuffer to hold the data of the Table.
var _buffer: StreamPeerBuffer
# Used to hold the position of the `_buffer` buffer.
var _pos: int
# Used to hold the vtable position.
var _vtable_start: int
# Used to hold the vtable size. 
var _vtable_size: int


func offset(vtableOffset: int) -> int:
	"""Offset provides access into the Table's vtable.
	Deprecated fields are ignored by checking the vtable's length.
	"""
	if vtableOffset < _vtable_size:
		return get_val(NumberTypes.VOffsetTFlags, _vtable_start + vtableOffset)
	return 0


func indirect(offset: int, buffer: StreamPeerBuffer = _buffer) -> int:
	"""Retrieve a relative offset.

	offset An `int` index into a ByteBuffer containing the relative offset.
	buffer from which the relative offset will be retrieved.
	Returns the relative offset stored at `offset`.
	"""
	return offset + Utils.get_val(NumberTypes.UOffsetTFlags, buffer, offset)


func string(off: int) -> String:
	"""string gets a string from data stored inside the flatbuffer."""
	off += get_val(NumberTypes.UOffsetTFlags, off)
	var start: int = off + NumberTypes.UOffsetTFlags.bytewidth
	var length: int = get_val(NumberTypes.Uint32Flags, off)
	_buffer.seek(start)
	return _buffer.get_string(length)


func vector_len(offset: int) -> int:
	"""Get the length of a vector.
	
	offset An `int` index into the Table's ByteBuffer.
	Returns the length of the vector whose offset is stored at `offset`.
	"""
	offset += _pos
	offset += get_val(NumberTypes.UOffsetTFlags, offset)
	return get_val(NumberTypes.UOffsetTFlags, offset)


func vector(offset: int) -> int:
	"""Get the start data of a vector.
	
	offset An `int` index into the Table's StreamPeerBuffer.
	Returns the start of the vector data whose offset is stored at `offset`.
	"""
	offset += _pos
	# data starts after the length
	offset += get_val(NumberTypes.UOffsetTFlags, offset)
	return offset + NumberTypes.Constants.SIZEOF_INT 


func union(t: Table, offset: int, buffer: StreamPeerBuffer = _buffer) -> Table:
	"""Initialize any Table-derived type to point to the union at the given `offset`.

	t A `Table`-derived type that should point to the union at `offset`.
	offset An `int` index into the Table's StreamPeerBuffer.
	buffer Table StreamPeerBuffer used to initialize the object Table-derived type.
	Returns the Table that points to the union at `offset`.
	"""
	t._reset(buffer, indirect(offset, buffer))
	return t


func _reset(buffer: StreamPeerBuffer = null, pos: int = 0):
	NumberTypes.enforce_value(NumberTypes.UOffsetTFlags, pos)
	
	self._buffer = buffer
	if self._buffer != null:
		self._pos = pos
		_vtable_start = self._pos - get_val(NumberTypes.SOffsetTFlags, self._pos)
		_vtable_size = get_val(NumberTypes.VOffsetTFlags, _vtable_start)
	else:
		self._pos = 0
		_vtable_start = 0
		_vtable_size = 0


func get_val(flags, offset: int):
	"""
	Get retrieves a value of the type specified by `flags`  at the
	given offset.
	"""
	NumberTypes.enforce_value(NumberTypes.UOffsetTFlags, offset)
	return Utils.get_val(flags, self._buffer, offset)
