extends Reference


enum Constants {
	SIZEOF_BYTE = 1,
	SIZEOF_SHORT = 2,
	SIZEOF_INT = 4,
	SIZEOF_LONG = 8,
	SIZEOF_FLOAT = 4,
	SIZEOF_DOUBLE = 8,
	FILE_IDENTIFIER_LENGTH = 4,
	SIZE_PREFIX_LENGTH = 4,
}


class BoolFlags:
	const bytewidth: int = 1
	const min_val = false
	const max_val = true
	const name: String = "bool"


class Uint8Flags:
	const bytewidth: int = 1
	const min_val = 0
	const max_val = pow(2, 8) - 1
	const name: String = "uint8"


class Uint16Flags:
	const bytewidth: int = 2
	const min_val = 0
	const max_val = pow(2, 16) - 1
	const name: String = "uint16"


class Uint32Flags:
	const bytewidth: int = 4
	const min_val = 0
	const max_val = pow(2, 32) - 1
	const name: String = "uint32"
	
	static func cast(val) -> int:
		return int(val)


class Uint64Flags:
	const bytewidth: int = 8
	const min_val = 0
	const max_val = pow(2, 64) - 1
	const name: String = "uint64"


class Int8Flags:
	const bytewidth: int = 1
	const min_val = -pow(2, 7)
	const max_val = pow(2, 7) - 1
	const name: String = "int8"


class Int16Flags:
	const bytewidth: int = 2
	const min_val = -pow(2, 15)
	const max_val = pow(2, 15) - 1
	const name: String = "int16"


class Int32Flags:
	const bytewidth: int = 4
	const min_val = -pow(2, 31)
	const max_val = pow(2, 31) - 1
	const name: String = "int32"


class Int64Flags:
	const bytewidth: int = 8
	const min_val = -pow(2, 63)
	const max_val = pow(2, 63) - 1
	const name: String = "int64"


class Float32Flags:
	const bytewidth: int = 4
	const min_val = null
	const max_val = null
	const name: String = "float32"


class Float64Flags:
	const bytewidth: int = 8
	const min_val = null
	const max_val = null
	const name: String = "float64"


class SOffsetTFlags:
	extends Int32Flags


class UOffsetTFlags:
	extends Uint32Flags


class VOffsetTFlags:
	extends Uint16Flags


static func enforce_value(flags, x):
	if flags.min_val == null && flags.max_val == null:
		return
	assert(x > flags.min_val || x < flags.max_val, 
							"bad number %s for type %s" % [str(x), flags.name])
