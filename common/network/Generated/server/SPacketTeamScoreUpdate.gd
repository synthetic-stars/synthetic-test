# automatically generated by the FlatBuffers compiler, do not modify

# namespace: server

class_name SPacketTeamScoreUpdate extends Table

static func get_root_as(buffer: StreamPeerBuffer,
								obj: SPacketTeamScoreUpdate,
								offset: int = -1) -> SPacketTeamScoreUpdate:
	buffer.set_big_endian(false)
	if offset == -1:
		offset = buffer.get_position()

	buffer.seek(offset)
	var pos: int = buffer.get_u32()

	obj._assign(buffer, pos + offset)
	return obj

func _assign(buffer: StreamPeerBuffer, offset: int) -> void:
	_reset(buffer, offset)

func team_color() -> int:
	var o: int = NumberTypes.UOffsetTFlags.cast(offset(4))
	if o != 0:
		return get_val(NumberTypes.Uint8Flags, self._pos + o)
	return 0

func score_delta() -> int:
	var o: int = NumberTypes.UOffsetTFlags.cast(offset(6))
	if o != 0:
		return get_val(NumberTypes.Int16Flags, self._pos + o)
	return 0

static func create_spacketteamscoreupdate(builder: FlatBuffersBuilder,
		team_color: int,
		score_delta: int) -> int:
	start(builder);
	add_score_delta(builder, score_delta)
	add_team_color(builder, team_color)
	return end(builder);

static func start(builder: FlatBuffersBuilder) -> void:
	builder.start_table(2)

static func add_team_color(builder: FlatBuffersBuilder, team_color: int) -> void:
	builder.prepend_uint8_slot(0, team_color, 0)

static func add_score_delta(builder: FlatBuffersBuilder, score_delta: int) -> void:
	builder.prepend_int16_slot(1, score_delta, 0)

static func end(builder: FlatBuffersBuilder) -> int:
	return builder.end_table()


