# Struttura delle mappe
Le mappe di SyntheticStars consistono semplicemente in scene Godot, che rispettano la seguente struttura:

![map_scene_tree](map_scene_tree.png)

- Map
	- 3DNodes
		- Lobby [^1]
		- BaseMap [^2]
	- Properties [^3]
		- LobbyPosition [^4]
		- SpawnPoints [^5]
			- Team1
			- Team2
		- BallPosition [^6]
		- Goals [^7]
			- Team1
			- Team2

[^1]: Nodo 3D contenente la sala d'attesa
[^2]: Nodo 3D contenente la mappa dove giocare la partita
[^3]: Nodo contenente le posizioni degli elementi di gioco e le proprietà della mappa, modificabili tramite l'ispettore
[^4]: Rappresenta la posizione della sala d'attesa
[^5]: Posizioni di (ri)nascita dei giocatori per le rispettive squadre
[^6]: Contiene il nodo o la scena da utilizzare come palla e la relativa posizione
[^7]: Aree a forma cilindrica da usare come porta per le rispettive squadre


All'avvio del server viene letto dal file di configurazione il nome della mappa, che viene usato per cercarla e caricarla dalla cartella delle mappe. 
Viene successivamente istanziata per essere poi popolata con i nodi figli che variano tra client e server. Nel primo caso `PlayerCamera`, `ClientPlayer`, `NetworkedPlayers` e nel secondo solo `Players`

# Creazione di una mappa
Per creare una mappa di gioco è assolutamente consigliato partire dalla scena chiamata `DefaultMap`, che esiste proprio per evitare di ricreare ogni volta la struttura della mappa, dimenticandosi dei pezzi.

Le fasi di creazione sono le seguenti:
1. Scaricare la scena `DefaultMap.tscn` e il client di gioco
2. Aprire il progetto del client
3. Copiare la scena precedentemente scaricata nella cartella `World`
4. Rinominare la scena con il nome della nuova mappa (es. `NewMap.tscn`)
5. Aprire la scena
6. Mettere dentro `Lobby` e `BaseMap` i vari nodi che li compongono (mesh, staticbody, ecc.)
7. Creare dentro il nodo `BaseMap` i vari nodi che comporranno la mappa o importare una mesh 3D creata con altri software
8. Cliccare sul nodo `Properties` e modificare tramite l'ispettore a destra il nome della mappa, le modalità di gioco supportate, i giocatori massimi e altre proprietà
9. Correggere la posizione nel mondo dei punti di rinascita per il `Team1` e il `Team2` e della sala d'attesa
10. Mettere dentro il nodo `Ball` i vari nodi che rappresenteranno la palla da catturare nel caso di modalità Touchdown
11. Correggere la posizione nel mondo del nodo `Ball` e delle porte (dentro nodo `Goals`) del `Team1` e del `Team2`
12. Aggiustare eventualmente il raggio del cilindro che viene usato come porta (nodo `CollisionShape` dentro `"Goals/Team1(2)"`)
 
### Note

---