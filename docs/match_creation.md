# Creazione di una partita
Prerequisiti:
- L'utente si è già autenticato su Nakama
- È già aperto un websocket tra il gioco e Nakama

Il websocket con Nakama viene aperto all'avvio del gioco se l'utente è già autenticato, viene usato per rendere disponibile la chat, per mostrare lo stato dell'utente agli amici (connesso, disconnesso, in gioco), ecc.

La creazione di una partita consiste internamente nelle seguenti fasi:
- **Richiesta di creazione della partita**: l'utente, tramite apposita interfaccia grafica, imposta le proprietà della nuova stanza (nome della mappa, modalità di gioco, ecc.) e ne conferma la creazione; la richiesta viene poi inoltrata tramite il websocket a Nakama.
- **Validazione della richiesta**: Nakama valida la richiesta controllando le proprietà (lunghezza massima e caratteri ammessi nel nome della partita, modalità di gioco, ecc.).
- **Creazione della partita**: se la richiesta è valida, viene creato su Nakama il "Match", ne vengono settate le proprietà e viene ritornato al client di gioco un oggetto contenente l'ID della partita appena creata  (il "Match" si occuperà di mantenere la lista dei giocatori in partita, di cancellare la partita se rimane vuota per X secondi ecc.).
- **Avvio del server di gioco**: dopo aver creato il "Match", Nakama contatta il GameserverManager chiedendo l'avvio di un'istanza del server e passandogli una lista di proprietà. Se tutto va a buon fine, il GameserverManager avvia l'istanza e ritorna a Nakama un oggetto contenente l'IP, la porta e l'ID di quest'ultima.
- **Richiesta di accesso alla partita**: appena ricevuto l'ID della partita da Nakama, il client richiede, attraverso lo stesso websocket, di entrare nella partita; se ci sono posti disponibili Nakama risponde con l'IP e la porta del server di gioco e un token di accesso con validità limitata (1 utilizzo).
- **Accesso al server di gioco**: dopo aver ricevuto i dati di accesso, il client si collega al server di gioco e presenta il token.
- **Validazione richiesta di accesso**: il server di gioco valida il token (JWT) e se è corretto (cioè contiene il nome del giocatore che ha fatto la richiesta e non è stato utilizzato più di una volta) permette all'utente di accedere, altrimenti lo butta fuori.