# Documentation guidelines


## 1. Single file documentation
Each file must contain, [after the "extends" or the "class_name" line](https://docs.godotengine.org/en/stable/tutorials/scripting/gdscript/gdscript_styleguide.html), a brief description of what it does, without just repeating the title words.

**e.g.**, **custom_kinematic_body.gd**:
it allows simulating, on a KinematicBody, some of the behaviors of a RigidBody like acceleration or force impulses.

#### 1.1 Methods and attributes documentation
It is not necessary to document every single function and its parameters, but the name of each of them and each attribute must be as understandable as possible. There should be no need for a comment to know what something represents or does. If a function does too much or is **too long** it's best to break it down further.

**e.g.**:
```python
for weapon in weapons:
	var config = get_weapon_config(weapon)
	var stats = {}
	var extras = {}
	
	# get the stats
	for stat_key in config.get_section_keys("stats"):
		stats[stat_key] = config.get_value("stats", stat_key)

	# get the extras 
	if config.has_section("extras"):
		var extras_keys = config.get_section_keys("extras")
		extras = {}

		for extra_key in extras_keys:
			var extra_value = config.get_value("extras", extra_key)
			extras[extra_key] = extra_value
```
**would become**
```python
for weapon in weapons:
	var config = get_weapon_config(weapon)
	var stats = get_stats(config)
	var extras = get_extras(config)
```

The names of things should not be redundant: it is useless to repeat something easily understandable from the context or the scope of the functions/variables.

Finally, when an empty dictionary gets initialized, specify its structure with a comment.


#### 1.2 Spacing
Spaces are fundamental for code navigability: aside the guidelines of the Godot[documentation](https://docs.godotengine.org/en/stable/tutorials/scripting/gdscript/gdscript_styleguide.html), others are defined here:
- put a space after the declaration of one or more variables, and, if there are many, also between the groups of logically connected ones;
- put a space before and after each code structure (conditions and iterations);
- put a space between the various switch cases;



## 2. Systems documentation
A system is a collection of classes that interact with each other to provide functionality. A system is like an API: it doesn't do anything by itself; it must be set up and used by control scripts. As these systems are more complex, they need a separate documentation file in the [docs](https://gitlab.com/synthetic-stars/synthetic-test/-/tree/main/docs) folder, in which we introduce what their function is and how their files interact with each other to make them work. The most important thing is to understand the general functioning of the system and its fundamental components; the description of the individual files and the code written according to these guidelines will act as detailed documentation.
