# Movement System

The movement system allows the player, as the name suggests, to move. A large quantity of possible movements gave rise to the need for a system that would allow adding new ones or modifying old ones in the simplest way possible.

The system is made up of two files: [movement_system.gd](https://gitlab.com/synthetic-stars/synthetic-test/-/blob/main/common/player/Movement/movement_system.gd "movement_system.gd") and the various [movement files](https://gitlab.com/synthetic-stars/synthetic-test/-/tree/main/common/player/Movement/Movements "movement files").
The MovementSystem has utility functions to move the player, that are used by most movements.

What triggers the system when an input is received is the [MovementStateMachine](https://gitlab.com/synthetic-stars/synthetic-test/-/blob/main/common/player/Movement/movement_state_machine.gd "movementstatemachine") - a child node of Player. It is nothing more than a state machine that coordinates the movements and manages the player's state. The status identifies how the player is currently moving (walking, falling, dashing, etc.).

All of these files are nodes and must be arranged as follows:
- Player
    - MovementStateMachine
    - MovementSystem
        - Movement1
        - Movement2
        - etc ...
