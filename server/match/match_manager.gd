class_name MatchManager extends Node

enum MatchState {
	WAITING, #waiting to reach min_player number
	READY, #min_player reached, the master can start the match
	PLAYING,
	ENDED,
}

signal match_state_change(new_state, old_state)
signal match_start()

onready var _configuration: Configuration = $"/root/Root/Configuration"
onready var _teams_manager: TeamsManager = $"/root/Root/TeamsManager"
onready var _map: Node = $"/root/Root/World/Map"

onready var match_state: int setget set_match_state

var gamemode: int

func _ready():
	match_state = MatchState.WAITING
	connect("match_start", self, "_on_match_start")


func _on_match_start() -> void:
	for team in _teams_manager.get_teams():
		var team_spawn_point: Vector3 = _map.get_spawn_point(team.team_color)
		var players = team.get_players()
		for player in players:
			player.teleport(team_spawn_point)


func master_start() -> void:
	if match_state == MatchState.READY:
		match_state = MatchState.PLAYING


#The match master quit, we need to choose a new one
func master_quit() -> void:
	pass


func player_join(player: NetworkedPlayer) -> void:
	var player_team: Team = _teams_manager.assign_to_team(player)
	if player_team != null:
		match match_state:
			MatchState.WAITING, MatchState.READY:
				player.teleport(_map.get_lobby_position())
			MatchState.PLAYING:
				player.teleport(_map.get_spawn_point(player_team.team_color))


func player_quit(player: NetworkedPlayer) -> void:
	_teams_manager.remove_from_team(player)
	
	if player.nakama_user_id.equals(_configuration.match_creator):
		master_quit()


func set_match_state(new_state: int) -> void:
	var old_state: int = match_state
	match_state = new_state
	
	match match_state:
		MatchState.PLAYING:
			emit_signal("match_start")
		
	emit_signal("match_state_change", new_state, old_state)
