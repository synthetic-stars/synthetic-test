class_name TeamsManager extends Node

signal team_changed(player, old_team, new_team)
signal quit_from_team(player, old_team)
signal min_players_reached()
signal min_players_not_reached()

onready var _configuration: Configuration = $"/root/Root/Configuration"
onready var _teams: Node = $"/root/Root/World/Teams"

var _max_team_size: int
var _players_in_teams: int = 0

func _ready():
	_max_team_size = _configuration.max_players / Team.get_teams_number()
	_create_teams()
	
	
func _create_teams() -> void:
	_teams.add_child(Team.new(Team.TeamColor.RED))
	_teams.add_child(Team.new(Team.TeamColor.BLUE))


#Function that assigns a player to a team when he joins the match
func assign_to_team(player: NetworkedPlayer) -> Team:
	if player.team == null:
		var new_team: Team = emptiest_team_strategy()
		#If the emptiest team is full assign the player to the NEUTRAL(Spectator) team
		if new_team.get_size() < _max_team_size:
			new_team.add_player(player)
			player.change_team_to(new_team)
			_players_in_teams += 1
			
			if _players_in_teams >= _configuration.min_players:
				emit_signal("min_players_reached")
			return new_team
		#else: #TODO
			#add to the NEUTRAL(Spectator) team
	return null


func remove_from_team(player: NetworkedPlayer) -> void:
	if player.team != null:
		var old_team: Team = player.team
		old_team.remove_player(player)
		player.change_team_to(null)
		_players_in_teams -= 1
		
		if _players_in_teams < _configuration.min_players:
			emit_signal("min_players_not_reached")
		
		emit_signal("quit_from_team", player, old_team)


#returns -1 if the teams hasn't changed, X >= 0 if the team has been changed to X
func change_team(player: NetworkedPlayer, new_team_color: int) -> int:
	var new_team: Team = get_team(new_team_color)
	var old_team: Team = player.team
	
	if old_team.get_size > 1: #the last player can't change team
		if new_team && new_team.get_size() < _max_team_size:
			old_team.remove_player(player)
			new_team.add_player(player)
			player.change_team_to(new_team)
			emit_signal("team_changed", player, old_team, new_team)
			return new_team.team_color
	
	return -1


func emptiest_team_strategy() -> Team:
	var min_value: int
	var teams_players: Dictionary
	
	for team in get_teams():
		#if two teams have the same number of players the last one is chosen
		teams_players[team.get_size()] = team
	
	min_value = teams_players.keys().min()
	return teams_players[min_value]


func get_team(team_color: int) -> Team:
	return _teams.get_node_or_null("Team%d" % [team_color]) as Team
 

func get_teams() -> Array:
	return _teams.get_children()
