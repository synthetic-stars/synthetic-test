class_name SyntheticStars extends Node

onready var configuration: Configuration = $Configuration
onready var match_manager: MatchManager = $MatchManager
onready var teams_manager: TeamsManager = $TeamsManager
onready var network_manager: NetworkManager = $Networking/NetworkManager

func _input(event):
	if Input.is_key_pressed(KEY_F11):
		OS.set_window_maximized(true)
