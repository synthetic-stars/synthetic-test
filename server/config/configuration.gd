class_name Configuration
extends Node

enum GameMode {
	TOUCH_DOWN,
	DEATH_MATCH,
}

enum WeaponType {
	ALL,
	GUNS,
	MELEE,
	BLACKLIST,
}

enum ConfigError {
	OK                    = 10,
	MAP_NAME_INVALID      = 15,
	MATCH_NAME_INVALID    = 20,
	GAME_MODE_INVALID     = 25,
	MATCH_CREATOR_NOT_SET = 30,
	WEAPON_TYPE_INVALID   = 35,
}

var MAPS_PATH = ProjectSettings.get_setting("server/general/map_list_path")

var map_name      : String
var match_name    : String
var game_mode     : int
var weapon_type   : int
var max_players   : int
var min_players   : int 
var match_creator : UUIDClass
var match_password: String
var min_level     : int
var map_names     : Array


func _ready():
	var load_error = load_settings()
	if load_error != ConfigError.OK:
		# Godot quits only after unnecessary loading all nodes
		get_tree().quit(load_error)


func load_settings() -> int:
	var regex_names: RegEx = RegEx.new()
	
	var map_name     : String
	var game_mode    : int
	var match_name   : String
	var match_creator: UUIDClass
	var match_passwd : String
	var weapon_type  : String
	var min_level    : int 
	var max_players  : int
	var min_players  : int
	
	if OS.has_feature("debug"):
		map_name = ProjectSettings.get_setting("server/match_settings/map_name")
		game_mode = GameMode.get(ProjectSettings.get_setting("server/match_settings/gamemode"), -1)
		match_name = ProjectSettings.get_setting("server/match_settings/match_name")
		match_creator = UUIDClass.new(ProjectSettings.get_setting("server/match_settings/match_creator"))
		match_passwd = ProjectSettings.get_setting("server/match_settings/match_password")
		weapon_type = ProjectSettings.get_setting("server/match_settings/weapon_type")
		min_level = ProjectSettings.get_setting("server/match_settings/min_level")
		max_players = ProjectSettings.get_setting("server/match_settings/max_players")
		min_players = ProjectSettings.get_setting("server/match_settings/min_players")
	else:
		map_name = OS.get_environment('MAP_NAME')
		game_mode = GameMode.get(OS.get_environment('GAME_MODE'), -1)
		match_name = OS.get_environment('MATCH_NAME')
		match_creator = UUIDClass.new(OS.get_environment('MATCH_CREATOR'))
		match_passwd = OS.get_environment('MATCH_PASSWORD')
		weapon_type = OS.get_environment('WEAPON_TYPE')
		min_level = int(OS.get_environment('MIN_LEVEL'))
		max_players = int(OS.get_environment('MAX_PLAYERS'))
		min_players = int(OS.get_environment('MIN_PLAYERS'))
	
	self.map_names = FilesUtils.read_file_lines(MAPS_PATH)
	
	regex_names.compile("^[a-zA-Z0-9._]{4,20}$")
	
	if map_name and self.map_names.has(map_name):
		self.map_name = map_name
	else:
		return ConfigError.MAP_NAME_INVALID
	
	if game_mode >= 0:
		self.game_mode = game_mode
	else:
		return ConfigError.GAME_MODE_INVALID
	
	if match_name and regex_names.search(match_name):
		self.match_name = match_name
	else:
		return ConfigError.MATCH_NAME_INVALID
	
	if match_creator:
		self.match_creator = match_creator
	else:
		return ConfigError.MATCH_CREATOR_NOT_SET
	
	self.weapon_type = WeaponType.get(weapon_type, WeaponType.ALL)
	
	if match_passwd:
		self.match_password = match_passwd
	
	if min_level: #TODO check if is in range
		self.min_level = min_level # 0 if not an int
	
	if min_players: #TODO check if is in range
		self.min_players = min_players # 0 if not an int
	
	if max_players: #TODO check if is in range
		self.max_players = max_players # 0 if not an int
	
	return ConfigError.OK
