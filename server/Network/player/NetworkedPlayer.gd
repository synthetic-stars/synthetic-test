extends Player
class_name NetworkedPlayer

var _connection_state: int
var username: String
var display_name: String
var nakama_user_id: UUIDClass
var team: Team

var target_pos : Vector3
var mov_dir

#TEMP var move away
var rng = RandomNumberGenerator.new()

func _enter_tree():
	rng.randomize()
	_connection_state = ConnectionState.NEW


func connection_init(access_token: String) -> int:
	_connection_state = ConnectionState.CONNECTING

	#Sostituire con verifica del JWT
	var is_access_token_valid: bool = true
	if is_access_token_valid:
		_connection_state = ConnectionState.CONNECTED

		# TEST CODE TO BE REMOVED
		username = "JudGenie"
		display_name = access_token #temp value
		nakama_user_id = UUIDClass.new("bedd7605-63a3-4f18-bbcd-4c7bc79a7a70")
		nakama_user_id.most_sig_bits = rng.randi()
	else:
		_connection_state = ConnectionState.FAILED

	return _connection_state


func _physics_process(delta):
	Debug.Print("player", "Energy: " + str(energy._energy))
	Debug.Print("player", "Health: " + str(health._health))
	if movement_state_machine.state != null:
		Debug.Print("player", "State: " + str(movement_state_machine.state))



func teleport(to: Vector3) -> void:
	set_translation(to)


#You must call also Team.remove_player(...) or Team.add_player(...)
func change_team_to(team: Team) -> void:
	self.team = team


func _short_to_degrees(short_val: int) -> float:
	return (short_val * (360.0 / 65536))


func add_force_relative_to_camera(force: Vector3) -> void:
	var relative_force := Vector3.ZERO

	# Faccio in modo che l'impulso sia relativo
	# alla rotazione della telecamera (quindi
	# x+=1 va a destra, x-=1 a sx, ecc).
	relative_force += Vector3(0,0,1).rotated(Vector3.UP, deg2rad(mov_dir)) * force.x
	relative_force += Vector3(0,1,0).rotated(Vector3.UP, deg2rad(mov_dir)) * force.y
	relative_force += Vector3(1,0,0).rotated(Vector3.UP, deg2rad(mov_dir)) * force.z

	_actual_velocity += relative_force
