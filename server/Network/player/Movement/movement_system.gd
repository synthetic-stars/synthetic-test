extends "res://common/player/Movement/movement_system.gd"


func get_input_direction() -> Vector3:
	var dir: Vector3 = Vector3.ZERO
	
	if player.mov_dir:
		var mov_dir: float = player.mov_dir
		
		var vect_1: Vector3 = Vector3(0,0,1).rotated(Vector3.UP, deg2rad(mov_dir))
		var vect_2: Vector3 = Vector3(1,0,0).rotated(Vector3.UP, deg2rad(mov_dir))
		
		if player.input_system.is_pressed("backward"):
			dir += vect_1
		if player.input_system.is_pressed("forward"):
			dir -= vect_1
		if player.input_system.is_pressed("right"):
			dir += vect_2
		if player.input_system.is_pressed("left"):
			dir -= vect_2

	return dir.normalized()
