class_name NetworkManager
extends Node

var _private_key: CryptoKey
var _server_cert: X509Certificate

var _configuration   : Configuration 
var _multiplayer_api : MultiplayerAPI
var _buffer_builder  : FlatBuffersBuilder
var _network_handler : NetHandler

var SERVER_PORT: int = ProjectSettings.get_setting("server/general/port")

func _ready():
	_configuration = $"/root/Root/Configuration"
	_buffer_builder = FlatBuffersBuilder.new(128)
	_network_handler = $NetHandler
	
	var err := _create_enet_server()
	if (err != OK):
		return err #TODO return in _ready doesn't do anything
	
	_network_handler.init(_multiplayer_api)
	return OK #TODO return in _ready doesn't do anything


func _create_enet_server() -> int: 
	var peer: NetworkedMultiplayerENet = NetworkedMultiplayerENet.new()
	
	peer.set_compression_mode(ProjectSettings.get_setting("networking/compression_mode"))
	peer.set_dtls_enabled(ProjectSettings.get_setting("networking/use_dtls"))
	if peer.is_dtls_enabled():
		_private_key = load(ProjectSettings.get_setting("networking/private_key_path"))
		_server_cert = load(ProjectSettings.get_setting("networking/certificate_path"))
		peer.set_dtls_certificate(_server_cert)
		peer.set_dtls_key(_private_key)
	
	#TODO change max_players if we add spectators
	var err = peer.create_server(SERVER_PORT, _configuration.max_players)
	if (err != OK):
		printerr("Error listening on ", SERVER_PORT)
		return err
	else:
		get_tree().set_network_peer(peer)
		_multiplayer_api = get_tree().get_multiplayer()
		return OK


################################################################################

func send_packet(id: int,
		mode: int,
		server_packet_type: int,
		server_packet_offset: int = 0) -> void:
			
	var servercontent_offset: int = ServerContent.create_servercontent(
		_buffer_builder,
		server_packet_type,
		server_packet_offset
	)
	
	_buffer_builder.finish(
		Packet.create_packet(
			_buffer_builder, 
			PacketContent.ServerContent, 
			servercontent_offset
		)
	)
	
	_multiplayer_api.send_bytes(
		_buffer_builder.sized_pool_byte_array(),
		id,
		mode
	)
	
	# TODO reset the buffer instead of recreating it
	_buffer_builder = FlatBuffersBuilder.new(128)


func send_spacketconnnectionaccepted(id: int, hello_message: String, team_color: int) -> void:
	var hello_message_offset: int = _buffer_builder.create_string(hello_message)
	var map_name_offset: int = _buffer_builder.create_string(_configuration.map_name)
	
	#Enable all teams
	var enabled_teams: int = 0
	for i in Team.TeamColor.size():
		enabled_teams |= 1 << (i + 1)
		
	send_packet(
		id,
		NetworkedMultiplayerENet.TRANSFER_MODE_RELIABLE,
		ServerPacketContent.SPacketConnectionAccepted,
		SPacketConnectionAccepted.create_spacketconnectionaccepted(
			_buffer_builder,
			hello_message_offset,
			team_color,
			enabled_teams,
			map_name_offset
		)
	)


func send_spacketplayerjoin(id: int,
		username: String,
		display_name: String,
		user_id: UUIDClass,
		rotation: Vector3,
		position: Vector3,
		team_color: int) -> void:
			
	var username_offset: int = _buffer_builder.create_string(username)
	var display_name_offset: int = _buffer_builder.create_string(display_name)
	
	SPacketPlayerJoin.start(_buffer_builder);
	SPacketPlayerJoin.add_display_name(_buffer_builder, display_name_offset)
	SPacketPlayerJoin.add_username(_buffer_builder, username_offset)
	SPacketPlayerJoin.add_user_id(
		_buffer_builder, 
		UUID.create_uuid(
			_buffer_builder,
			user_id.most_sig_bits,
			user_id.least_sig_bits
		)
	)
	SPacketPlayerJoin.add_rotation(
		_buffer_builder,
		EulerAngles.create_eulerangles(
			_buffer_builder,
			rotation.x,
			rotation.y,
			rotation.z
		)
	)
	SPacketPlayerJoin.add_position(
		_buffer_builder,
		Vec3f.create_vec3f(
			_buffer_builder,
			position.x,
			position.y,
			position.z
		)
	)
	SPacketPlayerJoin.add_team_color(_buffer_builder, team_color)
	var spacketplayerjoin_offset: int = SPacketPlayerJoin.end(_buffer_builder)
	
	send_packet(
		id,
		NetworkedMultiplayerENet.TRANSFER_MODE_RELIABLE,
		ServerPacketContent.SPacketPlayerJoin,
		spacketplayerjoin_offset
	)


func send_spacketplayerquit(source_id: int, user_id: UUIDClass) -> void:
	SPacketPlayerQuit.start(_buffer_builder);
	SPacketPlayerQuit.add_user_id(
		_buffer_builder,
		UUID.create_uuid(
			_buffer_builder,
			user_id.most_sig_bits,
			user_id.least_sig_bits
		)
	)
	var spacketplayerquit_offset: int = SPacketPlayerQuit.end(_buffer_builder)
	
	send_packet(
		-source_id, #send to everyone except the player that disconnected
		NetworkedMultiplayerENet.TRANSFER_MODE_RELIABLE,
		ServerPacketContent.SPacketPlayerQuit,
		spacketplayerquit_offset
	)


func send_spacketdisconnect(id: int, reason: String) -> void:
	var reason_offset: int = _buffer_builder.create_string(reason)
	
	var spacketdisconnect_offset: int = SPacketDisconnect.create_spacketdisconnect(
		_buffer_builder,
		reason_offset
	)
	
	send_packet(
		id, 
		NetworkedMultiplayerENet.TRANSFER_MODE_RELIABLE,
		ServerPacketContent.SPacketDisconnect,
		spacketdisconnect_offset
	)


func send_spacketgamestate(connected_players: Array) -> void:
	SPacketGameState.start_player_states_vector(_buffer_builder, connected_players.size())
	for connected_player in connected_players:
		var uuid: UUIDClass = connected_player.nakama_user_id
		var position: Vector3 = connected_player.get_translation()
		var head_rot = connected_player.head.get_rotation()
		PlayerGamestate.create_playergamestate(
			_buffer_builder,
			uuid.most_sig_bits,
			uuid.least_sig_bits,
			position.x,
			position.y,
			position.z,
			connected_player.health.get_health(),
			head_rot.x,
			head_rot.y,
			head_rot.z,
			connected_player.weapon_system.current
		)
	var player_states_offset: int = _buffer_builder.end_vector()
	var spacketgamestate_offset: int = SPacketGameState.create_spacketgamestate(
		_buffer_builder, 
		player_states_offset
	)
	send_packet(
		NetworkedMultiplayerENet.TARGET_PEER_BROADCAST,
		NetworkedMultiplayerENet.TRANSFER_MODE_UNRELIABLE_ORDERED,
		ServerPacketContent.SPacketGameState,
		spacketgamestate_offset
	)


func send_spacketteamchangeack(uuid: UUIDClass, new_team_color: int) -> void:
	var uuid_offset: int = UUID.create_uuid(_buffer_builder, uuid.most_sig_bits, uuid.least_sig_bits)
	var spacketteamchange_offset = SPacketTeamChangeAck.create_spacketteamchangeack(
		_buffer_builder, 
		uuid_offset,
		new_team_color
	)
	
	send_packet(NetworkedMultiplayerENet.TARGET_PEER_BROADCAST,
		NetworkedMultiplayerENet.TRANSFER_MODE_RELIABLE,
		ServerPacketContent.SPacketTeamChangeAck,
		spacketteamchange_offset
	)


func send_spacketteamscoreupdate(team_color: int, score_delta: int) -> void:
	var spacketteamscoreupdate_offset = SPacketTeamScoreUpdate.create_spacketteamscoreupdate(
		_buffer_builder,
		team_color,
		score_delta
	)
	
	send_packet(NetworkedMultiplayerENet.TARGET_PEER_BROADCAST,
		NetworkedMultiplayerENet.TRANSFER_MODE_RELIABLE,
		ServerPacketContent.SPacketTeamScoreUpdate,
		spacketteamscoreupdate_offset
	)


func send_spacketmatchstatechange(new_state: int) -> void:
	var spackettmatchstatechange_offset = SPacketMatchStateChange.create_spacketmatchstatechange(
		_buffer_builder,
		new_state
	)
	send_packet(NetworkedMultiplayerENet.TARGET_PEER_BROADCAST,
		NetworkedMultiplayerENet.TRANSFER_MODE_RELIABLE,
		ServerPacketContent.SPacketMatchStateChange,
		spackettmatchstatechange_offset
	)
