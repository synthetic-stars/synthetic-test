class_name ConnectionState

const NEW = 0
const CONNECTING = 1
const FAILED = 2
const CONNECTED = 3
