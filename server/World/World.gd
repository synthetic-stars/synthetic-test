extends Node

export (NodePath) onready var _network_manager = get_node(_network_manager)
export (NodePath) onready var _connected_players = get_node(_connected_players)

const update_limit: float = 1.0/20.0
var current_delta: float


func _process(delta):
	if current_delta > update_limit:
		if _connected_players.get_child_count():
			_network_manager.send_spacketgamestate(_connected_players.get_children())
			#print("Inviato gamestate a tutti i client")
		current_delta = 0
	current_delta += delta
