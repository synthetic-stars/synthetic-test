class_name MapLoader extends Node

onready var _configuration: Configuration = $"/root/Root/Configuration"
onready var _world : Node = $"/root/Root/World"

var MAP_BASE_PATH: String = ProjectSettings.get_setting("server/general/maps_folder_path")

var map_path: String

func _ready():
	map_path = "%s/%s.tscn" % [MAP_BASE_PATH, _configuration.map_name]
	load_map()


func load_map() -> void:
	var map_scene: PackedScene = load(map_path)
	if map_scene != null:
		_world.add_child(map_scene.instance())
	else:
		printerr("Cannot load the scene at %s" % [map_path])
		get_tree().quit(ERR_CANT_OPEN)
