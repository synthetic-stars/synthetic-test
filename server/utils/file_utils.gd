class_name FilesUtils

static func read_file_lines(path: String) -> Array:
	var list: Array = []
	var file: File = File.new()
	if file.open(path, File.READ) == OK:
		while file.get_position() < file.get_len():
			var line = file.get_line()
			list.append(line)
			
		file.close()
	else:
		printerr("Error opening the file %s" % [path])
	return list
