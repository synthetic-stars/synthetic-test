extends Node

onready var _synthetic_stars = $"/root/Root"
onready var _match_manager: MatchManager = $"/root/Root/MatchManager"
onready var _teams_manager: TeamsManager = $"/root/Root/TeamsManager"
onready var _network_manager = $"/root/Root/Networking/NetworkManager"
onready var _map: Node = $"/root/Root/World/Map"


func _ready():
	_teams_manager.connect("min_players_reached", self, "on_min_players_reached")
	_teams_manager.connect("min_players_not_reached", self, "on_min_players_not_reached")
	_teams_manager.connect("team_changed", self, "on_team_changed")
	
	_match_manager.connect("match_state_change", self, "on_match_state_change")
	
	for team in _teams_manager.get_teams():
		_map.connect_body_enter_team_goal(team.team_color, self, "on_body_enter_team_goal")
		team.connect("score_updated", self, "on_team_score_updated", [team.team_color])
	

func on_min_players_reached() -> void:
	if _match_manager.match_state == MatchManager.MatchState.WAITING:
		_match_manager.match_state = MatchManager.MatchState.READY
		print("Match ready")
		yield(get_tree().create_timer(20), "timeout")
		if _match_manager.match_state == MatchManager.MatchState.READY:
			_match_manager.match_state = MatchManager.MatchState.PLAYING


func on_min_players_not_reached() -> void:
	if _match_manager.match_state == MatchManager.MatchState.READY:
		_match_manager.match_state = MatchManager.MatchState.WAITING
		print("Match not ready anymore")


func on_match_state_change(new_state: int, old_state: int) -> void:
	_network_manager.send_spacketmatchstatechange(new_state)


func on_team_changed(player: NetworkedPlayer, old_team: Team, new_team: Team) -> void:
	_network_manager.send_spacketteamchangedack(player.nakama_user_id, new_team.team_color)


func on_team_score_updated(score_delta: int, team_color: int) -> void:
	_network_manager.send_spacketteamscoreupdate(team_color, score_delta)


func on_body_enter_team_goal(body: NetworkedPlayer, team_color: int):
	if _match_manager.match_state == MatchManager.MatchState.PLAYING:
		if team_color != body.team.team_color:
			var team: Team = _teams_manager.get_team(team_color)
			team.update_score(+1)
			print("%s touchdown at %d new score %d" % [body.username, team_color, team.score])
