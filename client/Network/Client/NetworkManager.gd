class_name NetworkManager
extends Node

var _server_cert: X509Certificate = load(ProjectSettings.get_setting("networking/certificate_path"))

var _multiplayer_api     : MultiplayerAPI
var _buffer_builder      : FlatBuffersBuilder
var _network_handler     : NetHandler

var _current_server_ip   : String
var _current_server_port : int

var connection_state: int

func init(server_ip: String, server_port: int) -> int:
	_buffer_builder = FlatBuffersBuilder.new(128)
	_network_handler = $NetHandler

	_current_server_ip = server_ip
	_current_server_port = server_port
	var err := _create_enet_client()
	if (err != OK):
		return err

	_network_handler.init(_multiplayer_api)
	return OK


func _create_enet_client() -> int:
	var peer := NetworkedMultiplayerENet.new()
	
	peer.set_compression_mode(ProjectSettings.get_setting("networking/compression_mode"))
	peer.set_dtls_enabled(ProjectSettings.get_setting("networking/use_dtls"))
	peer.set_dtls_hostname(ProjectSettings.get_setting("networking/dtls_hostname"))
	peer.set_dtls_certificate(_server_cert)

	var err: int = peer.create_client(_current_server_ip, _current_server_port)
	if (err != OK):
		print("Error connecting to ", _current_server_ip, ":", _current_server_port)
		connection_state = ConnectionState.FAILED
		return err
	else:
		get_tree().set_network_peer(peer)
		_multiplayer_api = get_tree().get_multiplayer()
		return OK


func close_connection() -> void:
	var peer: NetworkedMultiplayerENet = get_tree().get_network_peer()
	if peer:
		peer.close_connection()

################################################################################


func send_packet(mode: int,
		client_packet_type: int,
		client_packet_offset: int) -> void:

	var clientcontent_offset: int = ClientContent.create_clientcontent(
		_buffer_builder,
		client_packet_type,
		client_packet_offset
	)

	_buffer_builder.finish(
		Packet.create_packet(
			_buffer_builder,
			PacketContent.ClientContent,
			clientcontent_offset
		)
	)

	#_print_stream_buffer(_buffer_builder._buffer)
	_multiplayer_api.send_bytes(
		_buffer_builder.sized_pool_byte_array(),
		NetworkedMultiplayerENet.TARGET_PEER_SERVER,
		mode
	)
	#TODO reset the buffer instead of recreating it
	_buffer_builder = FlatBuffersBuilder.new(128)



func send_cpacketinput(head_target: Vector3,
		weapon: int,
		input_actions: Dictionary,
		mov_dir: int
		) -> void:
	CPacketInput.start_input_actions_vector(_buffer_builder, input_actions.size())
	
	var i: int = 0
	for input_action in input_actions:
		InputAction.create_inputaction(_buffer_builder,
			i,
			input_actions[input_action]
		)
		i += 1
		
	var player_actions_offset: int = _buffer_builder.end_vector()
	CPacketInput.start(_buffer_builder)
	CPacketInput.add_head_target(
		_buffer_builder,
		Vec3f.create_vec3f(
			_buffer_builder,
			head_target.x,
			head_target.y,
			head_target.z
		)
	)
	
	CPacketInput.add_mov_dir(
		_buffer_builder,
		mov_dir
	)
	
	CPacketInput.add_input_actions(
		_buffer_builder,
		player_actions_offset
	)
	CPacketInput.add_weapon(_buffer_builder, weapon)
	
	var cpacketinput_offset: int = CPacketInput.end(_buffer_builder)
	send_packet(
		NetworkedMultiplayerPeer.TRANSFER_MODE_UNRELIABLE_ORDERED,
		ClientPacketContent.CPacketInput,
		cpacketinput_offset
	)


func _vector_normal_to_short(value: float) -> int:
	return (int((value)*65536/2) & 65535)


func send_cpacketconnectioninit(procol_version: int, access_token: String) -> void:
	var access_token_offset = _buffer_builder.create_string(access_token)

	var cpacketconnectioninit_offset: int = CPacketConnectionInit.create_cpacketconnectioninit(
		_buffer_builder,
		procol_version,
		access_token_offset
	)
	send_packet(
		NetworkedMultiplayerPeer.TRANSFER_MODE_RELIABLE,
		ClientPacketContent.CPacketConnectionInit,
		cpacketconnectioninit_offset
	)


func _print_stream_buffer(stream_buffer: StreamPeerBuffer):
	var available_bytes: int = stream_buffer.get_available_bytes()
	print("Size: %d, available: %d" % [stream_buffer.get_size(), available_bytes])
	var output: String = "["
	for i in range(0, available_bytes - 1):
		output += str(stream_buffer.get_8()) + ", "
	print("%s%d]" % [output, stream_buffer.get_8()])
	stream_buffer.seek(stream_buffer.get_size() - available_bytes)
