class_name NetHandler
extends Node

#Move away
const PROTOCOL_VERSION = 123
const NETWORKED_PLAYER_SCENE: PackedScene = preload("res://Network/player/NetworkedPlayer.tscn")

export (NodePath) onready var _local_player = get_node(_local_player)

var _synthetic_stars#: SyntheticStars
var _network_manager#: NetworkManager
var _multiplayer_api: MultiplayerAPI
var _buffer: StreamPeerBuffer
var _networked_players: Node

func _ready():
	_synthetic_stars = get_node("/root/Root")
	_network_manager = get_parent() #as NetworkManager
	_buffer = StreamPeerBuffer.new()
	_networked_players = get_node("/root/Root/World/NetworkedPlayers")


func init(multiplayer_api: MultiplayerAPI):
	_multiplayer_api = multiplayer_api
	_connect_main_signals()


func _connect_main_signals():
	_multiplayer_api.connect("connected_to_server", self, "_connected_to_server")
	_multiplayer_api.connect("server_disconnected", self, "_player_disconnected")
	_multiplayer_api.connect("network_peer_packet", self, "_on_packet")


func _connected_to_server():
	var nakama_session: NakamaSession = get_node("/root/Root/Networking/Nakama").session
	if nakama_session:
		var nakama_token = get_node("/root/Root/Control/NakamaLogin/AccessToken")
		_network_manager.send_cpacketconnectioninit(PROTOCOL_VERSION, nakama_token.text)
		print("Connesso correttamente al server")
	else:
		_network_manager.close_connection()


func _player_disconnected():
	_network_manager.connection_state = ConnectionState.NEW
	print("Disconnesso dal server")

# Packet handling methods

func _on_packet(id: int, packet_bytes: PoolByteArray):
	#print("Ricevuto un pacchetto da %d" % [id])
	_buffer.seek(0)
	_buffer.data_array = packet_bytes

	#_print_stream_buffer(_buffer)
	var packet: Packet = Packet.get_root_as(_buffer, Packet.new())
	var packet_content_type = packet.packet_content_type()

	if (packet != null && packet_content_type == PacketContent.ServerContent):
		var server_packet: ServerContent = packet.packet_content(ServerContent.new())
		var server_packet_type = server_packet.content_type()

		if (server_packet != null && server_packet_type >= 0):
			match server_packet_type:
				ServerPacketContent.SPacketConnectionAccepted:
					_connection_accepted_handler(
						server_packet.content(SPacketConnectionAccepted.new())
					)

				ServerPacketContent.SPacketDisconnect:
					_disconnect_handler(
						server_packet.content(SPacketDisconnect.new())
					)

				ServerPacketContent.SPacketPlayerJoin:
					_player_join_handler(
						server_packet.content(SPacketPlayerJoin.new())
					)

				ServerPacketContent.SPacketPlayerQuit:
					_player_quit_handler(
						server_packet.content(SPacketPlayerQuit.new())
					)

				ServerPacketContent.SPacketGameState:
					_gamestate_handler(
						server_packet.content(SPacketGameState.new())
					)
				
				ServerPacketContent.SPacketTeamChangeAck:
					var pkt: SPacketTeamChangeAck = server_packet.content(SPacketTeamChangeAck.new())
					var uuid: UUIDClass = UUIDClass.new().from_most_least_sig_bits(
						pkt.user_id().high_bytes, pkt.user_id().low_bytes()
					)
					var new_team_color: int = pkt.new_team_color()
					if new_team_color != -1:
						print(uuid.to_string(), " changed team to ", Team.TeamColor[new_team_color])
					else:
						print("The team of %s remained unchanged" % [uuid.to_string()])
				
				ServerPacketContent.SPacketTeamScoreUpdate:
					var pkt: SPacketTeamScoreUpdate = server_packet.content(SPacketTeamScoreUpdate.new())
					var team: Team = _synthetic_stars.teams_manager.get_team(pkt.team_color())
					team.score += pkt.score_delta()
					print("The score of the team %d has been updated to %d" % [team.team_color, team.score])
				
				ServerPacketContent.SPacketMatchStateChange:
					var pkt: SPacketMatchStateChange = server_packet.content(SPacketMatchStateChange.new())
					print("The match changed state to ", pkt.new_state())
				
				ServerPacketContent.SPacketMatchMasterChange:
					var pkt: SPacketMatchMasterChange = server_packet.content(SPacketMatchMasterChange.new())
					var uuid: UUIDClass = UUIDClass.new()
					uuid.from_most_least_sig_bits(pkt.new_master_id().high_bytes(), pkt.new_master_id().low_bytes())
					print("The new match master is ", uuid.to_string())

				_:
					printerr("Ricevuto pacchetto con id non valido")


func _connection_accepted_handler(packet: SPacketConnectionAccepted):
	if not packet:
		return

	#TODO add only enabled teams 
	_synthetic_stars.teams_manager.create_teams(packet.teams_enabled())
	print("Team assigned to you ", packet.team_color())
	_network_manager.connection_state = ConnectionState.CONNECTED
	print("Ricevuto connection accepted packet Hello MSG: %s Mappa: %s" % [packet.hello_message(), packet.map_name()])


func _disconnect_handler(packet: SPacketDisconnect):
	if not packet:
		return

	print("Ricevuto pacchetto di disconnessione Motivazione %s" % packet.message())
	_network_manager.close_connection()


func _player_join_handler(packet: SPacketPlayerJoin):
	if not packet:
		return

	var new_player: NetworkedPlayer = NETWORKED_PLAYER_SCENE.instance()
	var uuid: UUID = packet.user_id()

	new_player.user_id = UUIDClass.new().from_most_least_sig_bits(
		uuid.high_bytes(), uuid.low_bytes()
	)
	new_player.name = str(new_player.user_id)
	new_player.username = packet.username()
	new_player.display_name = packet.display_name()

	var rotation_degrees = packet.rotation()
	
	if rotation_degrees:
		rotation_degrees = Vector3(
			_short_to_degrees(rotation_degrees.yaw()),
			_short_to_degrees(rotation_degrees.pitch()),
			_short_to_degrees(rotation_degrees.roll())
		)
		new_player.set_rotation_degrees(rotation_degrees)

	var position = packet.position()

	if position:
		position = Vector3(
			position.x(),
			position.y(),
			position.z()
		)
		new_player.set_translation(position)
	
	_synthetic_stars.teams_manager.add_to_team(new_player, packet.team_color())
	
	_networked_players.add_child(new_player)


func _player_quit_handler(packet: SPacketPlayerQuit):
	if not packet:
		return

	var uuid = packet.user_id()
	uuid = UUIDClass.new().from_most_least_sig_bits(uuid.high_bytes(), uuid.low_bytes())

	var player: NetworkedPlayer = _networked_players.get_node(str(uuid))
	_synthetic_stars.teams_manager.remove_from_team(player)
	
	_networked_players.remove_child(player)
	player.queue_free()


func _gamestate_handler(packet: SPacketGameState):
	if not packet:
		return

	if not packet.player_states_is_none():
		var player_states_length: int = packet.player_states_length()

		if player_states_length > 0:
			for i in range(player_states_length):
				var player_state: PlayerGamestate = packet.player_states(i)
				_player_gamestate_handler(player_state)


func _player_gamestate_handler(player_gamestate: PlayerGamestate):
	if not player_gamestate:
		return

	var uuid = player_gamestate.uuid()

	if uuid:
		uuid = UUIDClass.new().from_most_least_sig_bits(
			uuid.high_bytes(),
			uuid.low_bytes()
		)
		
		var connected_player: Spatial = _networked_players.get_node_or_null(str(uuid))

		if not connected_player:
			connected_player = _local_player
			var health = player_gamestate.health()
			if health:
				connected_player.health.set_health(health)

		var position = player_gamestate.position()

		if position:
			position = Vector3(
				position.x(),
				position.y(),
				position.z()
			)
			connected_player.set_translation(position)
		
		

###############################################################################

func _print_stream_buffer(stream_buffer: StreamPeerBuffer):
	var available_bytes: int = stream_buffer.get_available_bytes()
	print("Size: %d, available: %d" % [stream_buffer.get_size(), available_bytes])
	var output: String = "["
	
	for i in range(0, available_bytes - 1):
		output += str(stream_buffer.get_8()) + ", "

	print("%s%d]" % [output, stream_buffer.get_8()])
	stream_buffer.seek(stream_buffer.get_size() - available_bytes)


func _short_to_degrees(short_val: int) -> float:
	return (short_val * (360.0 / 65536))
