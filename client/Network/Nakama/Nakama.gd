extends Node

var scheme = "https"
var host = "nakama.syntheticstars.org"
var port = 7350

var socket: NakamaSocket
var client: NakamaClient
# TODO remove this fake initialization
var session: NakamaSession = NakamaSession.new("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c")

func init(access_token: String):
	client = Nakama.create_client("", host, port, scheme, 200)
	session = NakamaSession.new(access_token)
	
	#var account = yield(client.get_account_async(session), "completed")
	#if account.user != null and account.wallet != null:
	#	print(account.user.id)
	#	print(account.user.username)
	#	print(account.custom_id)
	
	socket = Nakama.create_socket_from(client)
	socket.connect("connected", self, "_on_socket_connected")
	socket.connect("closed", self, "_on_socket_closed")
	socket.connect("received_error", self, "_on_socket_error")
	socket.connect("received_match_state", self, "_on_match_data")
	yield(socket.connect_async(session), "completed")
	print("Done")
	
	#var created_match: NakamaRTAPI.Match = yield(socket.create_match_async(), "completed")
	#print(created_match)
	
	var rpc: NakamaAPI.ApiRpc = yield(socket.rpc_async("server.create_room", {"roomName": "example"}), "completed")
	var create_room_result = JSON.parse(rpc.payload).result
	
	var match_list = yield(client.list_matches_async(session, 0, 10, 10, true, "", ""), "completed")
	print(match_list)
	
	#var match_id = create_room_result["result"]["matchid"]
	#var match_joined: NakamaRTAPI.Match = yield(socket.join_match_async(match_id), "completed")
	#print(match_joined)


func _on_socket_connected():
	print("Socket connected.")

func _on_socket_closed():
	print("Socket closed.")

func _on_socket_error(err):
	printerr("Socket error %s" % err)

func _on_match_data(p_state : NakamaRTAPI.MatchData):
  print("Received match state with opcode %s, data %s" % [p_state.op_code, parse_json(p_state.data)])
