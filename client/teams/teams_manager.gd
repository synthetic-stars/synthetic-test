class_name TeamsManager extends Node

signal team_changed(player, old_team, new_team)
signal quit_from_team(player, old_team)

onready var _teams: Node = $"/root/Root/World/Teams"


func create_teams(enabled_teams: int) -> void:
	_teams.add_child(Team.new(Team.TeamColor.RED))
	_teams.add_child(Team.new(Team.TeamColor.BLUE))


func add_to_team(player: NetworkedPlayer, team_color: int) -> void:
	if player.team == null:
		var team: Team = get_team(team_color)
		team.add_player(player)
		player.change_team_to(team)


func remove_from_team(player: NetworkedPlayer) -> void:
	if player.team != null:
		var old_team: Team = player.team
		old_team.remove_player(player)
		player.change_team_to(null)
		emit_signal("quit_from_team", player, old_team)


func change_team(player: NetworkedPlayer, new_team_color: int) -> void:
	var new_team: Team = get_team(new_team_color)
	var old_team: Team = player.team
	
	old_team.remove_player(player)
	new_team.add_player(player)
	player.change_team_to(new_team)
	emit_signal("team_changed", player, old_team, new_team)


func get_team(team_color: int) -> Team:
	return _teams.get_node_or_null("Team%d" % [team_color]) as Team
 

func get_teams() -> Array:
	return _teams.get_children()
