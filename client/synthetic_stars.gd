class_name SyntheticStars extends Node

onready var teams_manager: TeamsManager = $TeamsManager
onready var network_manager: NetworkManager = $Networking/NetworkManager

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func _unhandled_input(event) -> void:
	if Input.is_action_just_pressed("ui_cancel"):
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	if event is InputEventMouseButton:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		
	if Input.is_key_pressed(KEY_F11):
		OS.set_window_maximized(true)
