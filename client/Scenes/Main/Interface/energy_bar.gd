extends ProgressBar


export(NodePath) onready var player = get_node(player)


func _process(delta):
	value = player.energy._energy
