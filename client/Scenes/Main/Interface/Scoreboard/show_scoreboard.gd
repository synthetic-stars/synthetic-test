extends PanelContainer


func _physics_process(delta: float) -> void:
	if Input.is_action_pressed("ui_focus_next"):
		self.visible = true
	else:
		self.visible = false
