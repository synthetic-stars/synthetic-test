extends Player

var packet_limit: float = 1.0/60.0
var limit: float
var crosshair : TextureRect
var camera : Camera

var target_pos : Vector3
var team: Team

func _ready():
	crosshair = $"../FollowingCamera/Crosshair"
	camera = $"../FollowingCamera/Interpolated/Camera"


func _process(delta: float):
	# Bisogna eseguirlo ogni step altrimenti:
	# 1. non funziona la rotazione del giocatore se non sei collegato al server
	# 2. è molto più scattosa
	_process_camera_rot()

	if network_manager.connection_state == ConnectionState.CONNECTED:
		if (limit > packet_limit):
			_send_inputs()
			limit = 0
		limit += delta


func _physics_process(_delta: float):
	Debug.Print("player", "Energy: " + str(energy._energy))
	Debug.Print("player", "Health: " + str(health._health))
	if movement_state_machine.state != null:
		Debug.Print("player", "State: " + str(movement_state_machine.state))


func _process_camera_rot():
	var ch_pos = crosshair.rect_position + crosshair.rect_size * 0.5

	var ray_from_camera = camera.project_ray_origin(ch_pos)
	var ray_dir_camera = camera.project_ray_normal(ch_pos)
	#DrawLine3d.DrawRay(ray_from_camera, ray_from_camera + ray_dir_camera * 1000, Color(0, 0, 1), 2)
	var shoot_target
	var result = RaycastUtils.raycast(ray_from_camera, ray_from_camera + ray_dir_camera * 1000, [self])
	if result.empty():
		shoot_target = ray_from_camera + ray_dir_camera * 1000
	else:
		shoot_target = result.position

	look_at(shoot_target, Vector3.UP)
	rotation_degrees.x = 0
	rotation_degrees.z = 0
	head.look_at(shoot_target, Vector3.UP)

	target_pos = shoot_target # Bisogna impostare questa variabile globale in quanto poi è usata nelle armi per sparare


func _send_inputs() -> void:
	var weapon: int = weapon_system.current
	var rotation_degrees: Vector3 = head.get_rotation_degrees()
	rotation_degrees.x = _degree_to_short(rotation_degrees.x)
	rotation_degrees.y = _degree_to_short(rotation_degrees.y)
	rotation_degrees.z = _degree_to_short(rotation_degrees.z)

	network_manager.send_cpacketinput(
		target_pos, #head_pos
		weapon,
		input_system._input_system._actions,
		_degree_to_short(camera.rotation_degrees.y) #mov_dir
	)

	input_system._input_system.reset_actions()


#Temp function, move away
func _degree_to_short(degree: int) -> int:
	return (int((degree)*65536/360) & 65535)


func add_force_relative_to_camera(force: Vector3) -> void:
	var relative_force := Vector3.ZERO
	var basis := camera.transform.basis

	# Faccio in modo che l'impulso sia relativo
	# alla rotazione della telecamera (quindi
	# x+=1 va a destra, x-=1 a sx, ecc).
	relative_force += basis.x * force.x
	relative_force += basis.y * force.y
	relative_force += basis.z * force.z

	_actual_velocity += relative_force
