extends "res://common/player/Movement/movement_system.gd"


func get_input_direction() -> Vector3:
	var dir = Vector3.ZERO

	if player.input_system.is_pressed("backward"):
		dir += player.camera.transform.basis.z
	if player.input_system.is_pressed("forward"):
		dir -= player.camera.transform.basis.z
	if player.input_system.is_pressed("right"):
		dir += player.camera.transform.basis.x
	if player.input_system.is_pressed("left"):
		dir -= player.camera.transform.basis.x
	
	return dir.normalized()
	
